import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CensorComponent } from './censor/censor.component';

const routes: Routes = [{ path: '', component: CensorComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

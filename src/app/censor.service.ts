import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface Redaction {
  redactTargets: string[];
  documentText: string;
}

const documentCensorApi = `http://localhost:3000`;
@Injectable({
  providedIn: 'root',
})
export class CensorService {
  constructor(private http: HttpClient) {}

  censorDocument$(
    redactTargets: string,
    documentText: string
  ): Observable<any> {
    return this.http.post<Redaction>(`${documentCensorApi}`, {
      redactTargets,
      documentText,
    });
  }
}

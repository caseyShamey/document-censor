import { Component, NgZone, ViewChild } from '@angular/core';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take, tap } from 'rxjs';
import { CensorService } from '../censor.service';

@Component({
  selector: 'app-censor',
  templateUrl: './censor.component.html',
  styleUrls: ['./censor.component.css'],
})
export class CensorComponent {
  public redactTargets!: string;
  public censoredText!: string;
  public documentText!: string;

  constructor(
    private readonly censorService: CensorService,
    private _ngZone: NgZone
  ) {}
  @ViewChild('autosize') autosize!: CdkTextareaAutosize;

  censorText(): void {
    this.censorService
      .censorDocument$(this.redactTargets, this.documentText)
      .pipe(tap((res) => (this.censoredText = res.censoredText)))
      .subscribe();
  }

  triggerResize() {
    this._ngZone.onStable
      .pipe(
        take(1),
        tap(() => {
          this.autosize.resizeToFitContent(true);
        })
      )
      .subscribe();
  }
}
